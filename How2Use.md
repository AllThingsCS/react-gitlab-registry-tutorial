Glossary

`ACCESS_TOKEN` is used to provide access to a repository. Instructions on locating the ACCESS_TOKEN be found below. More information can be found here.

`PROJECT_ID` is the ID associated with the repository. Instructions on obtaining the project ID can be found below.

`GITLAB_TOKEN` is a variable which is defined by the developer when assigning this variable to a ACCESS_TOKEN inside the CI/CD variables. This will allow reference to this variable when it is used in the project. i.e.,  ${GITLAB_TOKEN}

`SCOPE` is the location in which the package library is stored. These are also known as groups such as dev, ci-skills, etc…


## Set up and Installation

Setting required pre-requisites → Set up the following files and their respective data.

Note: Ensure items with `< ... >` are replace with their designated values.

Create → `.npmrc`

```
@${SCOPE}:registry=https://gitlab.example.com/api/v4/packages/npm/
//gitlab.example.com/api/v4/projects/${PROJECT_ID_EXAMPLE}/packages/npm/:_authToken=${GITLAB_TOKEN}
//gitlab.example.com/api/v4/packages/npm/:_authToken=${GITLAB_TOKEN}
```

You will also NEED to assign these variables inside the: Settings → CI/CD → Variables. The pipeline will fail if this is not completed.

note: the PROJECT_ID and SCOPE can be hardcoded in the .npmrc but avoid hardcoding the GITLAB_TOKEN.

Create → `.bashrc`

```
# You must run `source .bashrc` in your project root terminal every time you have a new terminal open
# Without this, you will not be able to install your packages

export GITLAB_TOKEN=<GITLAB_TOKEN>
export PROJECT_ID=<PROJECT_ID_EXAMPLE>
export SCOPE=<SCOPE>
```

BE SURE TO gitignore the `.bashrc` file

- note: you will NEED to run source .bash inside your project root terminal every new session.

Install with Yarn

`yarn add @<SCOPE>/<PACKAGE_NAME>`


Install with Npm

`npm i @<SCOPE>/<PACKAGE_NAME>`

NOTE: this can also be found in.

- Packages & Registries → Package Registry → “Select Latest package” → “Copy the line under Installation”
