Before getting started

This tutorial assumes you already have a Gitlab Package Registry set up, a npm or yarn initialized, and an gitlab access token set in your project. However, if this isn’t the case then all the instructions for doing so can be found the How2SetupRegistry docs.

Disclaimer

There will be a lot of copy and paste in this tutorial in order to maximize productivity. Each copy and paste will be sectional per the area it pertains. Details will be added in the form of comments within the code snippets or under the section title. 

- note: this tutorial will be using Yarn but feel free to use npm where ever you see fit.

### Create a react app test suite

Create a react-app by running this command in the project root terminal.

`yarn create react-app react-app-test-suite`

**FILE**
`Package.json`

These are the required dependencies and scripts you will want for this project. You may have additional dependencies for your project and that is okay. (Versioning is not important).

note: DO NOT INCLUDE REACT OR REACT-DOM IN THE dependencies. THEY MUST RESIDE IN peerDependencies. If you do this then you may encounter React Hook errors when importing external libraries.

```
{
  ...
  "main": "dist/index.cjs.js",
  "module": "dist/index.esm.js",
  "files": [ "dist" ],
  "scripts": {
    "build": "rollup -c",
    "build-watch": "rollup -c -w",
    "start-test-suite": "cd react-app-test-suite && yarn run start",
    "install-all": "yarn install && cd react-app-test-suite && yarn install",
    "dev": "npm-run-all --parallel build-watch start-test-suite"
  },
  "peerDependencies": {
    "react": "^16.8.0",
    "react-dom": "^16.8.0"
  },
  "devDependencies": {
    "@babel/cli": "^7.15.4",
    "@babel/core": "^7.15.5",
    "@babel/preset-env": "^7.15.4",
    "@babel/preset-react": "^7.14.5",
    "@rollup/plugin-babel": "^5.3.0",
    "@rollup/plugin-image": "^2.1.1",
    "npm-run-all": "^4.1.5",
    "rollup": "^2.56.3",
    "rollup-plugin-delete": "^2.0.0",
    "rollup-plugin-peer-deps-external": "^2.2.4"
  },
  ...
}
```

**FILE**
`.babelrc`

Is necessary for bundling React.
```
{
  "presets": ["@babel/env", "@babel/preset-react"]
}
```

**FILE**
`rollup.config.js`

Rollup is used to bundle the javascript into the dist folder so it might be exported from there.

```
import pkg from "./package.json";
import del from "rollup-plugin-delete";
import external from "rollup-plugin-peer-deps-external";
import babel from "@rollup/plugin-babel";
import image from '@rollup/plugin-image';

export default [
  {
    input: "src/index.js",
    output: [
      {
        file: "react-app-test-suite/src/exported-components/index.js",
        format: "esm",
        banner: "/* eslint-disable */",
      },
      { file: pkg.main, format: "cjs" },
      { file: pkg.module, format: "esm" },
    ],
    plugins: [
      image(), // only neccessary if you are including images in your library
      external(),
      babel({ exclude: "node_modules/**", babelHelpers: "bundled" }),
      del({ targets: ["dist/*", "react-app-test-suite/src/exported-components"] }),
    ],
    external: Object.keys(pkg.peerDependencies || {}),
  },
];
```

**FILE**
src → exported-components → `Blobby.js`

note: if you use this test widget then you will need to add styled-components in the root project
- `yarn add styled-components`

```
import React from "react";
import styled from "styled-components";

const Blob = styled.div`
  border-radius: 50px;
  background-color: #72a0c1;
  color: #e52b50;
  position: relative;
  height: 100px;
  width: 100px;
  text-align: center;
`;

const Blobby = () => (
  <Blob>
    <p>/-(_)-\</p>
    <p>+-^U_U^-+</p>
    <p> \|/\|/ </p>
  </Blob>
);

export default Blobby;
```

**FILE**
src → `index.js`

```
import Blobby from "./exported-components/Blobby.js"

export { Blobby };
```

**FILE**
react-app-test-suite → src → `App.js`

note: you’ll need to delete everything that is currently in App.js

```
import React from "react";

import { Blobby } from "./exported-components/index"

function App() {

  return (
    <div>
      <Blobby />
    </div>
  );
}

export default App;
```

Terminal Commands to be ran in the project root

- `yarn run install-all` - installs all node_modules for the package.json dependencies which were added.

- `yarn run build` - will build the react into the dist folder and the react-app-test-suite 's src directory.

- `yarn run dev` - The react-app will start on localhost.

