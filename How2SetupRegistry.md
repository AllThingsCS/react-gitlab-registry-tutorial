In this confluence, you learn how to create a NPM Gitlab Package Registry, import said package, and what to do when running pipelines on a repository where the package resides.

Reference this on how to use a NPM Gitlab Package Registry.

Creating a Gitlab Package

Set up your repository

New Project → Create blank project → “fill out form“ → Create project

Clone your repository to a local directory with the value within your repository.

git clone `<git@gitlab... etc>`  Input this in your desired location within your local machine.

Initialize NPM or Yarn

Use Either

- npm init

- yarn init

    - note: there is no need to fill in any of the questions

Create index.js in root project and paste the following.

- console.log("hello world")


**FILE**
Inside `package.json` add the following

```
{
  "name": "@foo/<PROJECT_NAME>",
  ...
  "version": "1.0.0",
  ...
  "publishConfig": {
    "@foo:registry": "https://gitlab.example.com/api/v4/projects/<PROJECT_ID>/packages/npm/"
  },
  "files": [ "index.js"],
  ...
}
```

`foo` is the scope of your project. Refer the example below. This is going to be the group you made your repository in.

`<PROJECT_ID>` is found in your repository’s main page.

- note: you will not be able to npm publish without the "publishConfig": ...

**FILE**
Create `.gitlab-ci.yml` in your project root and add the following to this file.

```
image: node:latest

stages:
  - deploy

deploy:
  stage: deploy
  script:
    - echo "//${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/packages/npm/:_authToken=${CI_JOB_TOKEN}">.npmrc
    - npm publish
  only:
    - master # you may need to change this.
```

Ensure a runner is enabled for your repository

Settings → CI/CD → “Runners” Expand → “Enable Runner”

Create a Gitlab Token

- Settings → Access Tokens → “Fill out form and [x] api” → Create project access token → “COPY TOKEN”

Insert Gitlab Token into CI/CD variables

- Settings → CI/CD → “Variables” Expand → Add variable → “Create a key name of your choice (GITLAB_TOKEN recommended) and paste the copied token value into” Value “field” → Add variable

Add, Commit, and Push your code

`git status`

`git add .` 

`git commit -m "Insert your commit message here"`

`git push --set-upstream origin <BRANCH_NAME>`  or git push and follow git instructions

Follow the merge request link and create the merge request. Merge if everything looks good to you.

Make sure the pipeline succeeds


If the pipeline fails and there is a RED X

- Click on the little red X and then click again on the button that says “X deploy”

- Next, read through the log to determine what is causing the problem. If you something to the effect of Not found then you might be missing the publishConfig within your package.json.

 = It is also possible the pipeline failing has nothing to do with the publishing of the package. If npm publish passes, it is likely something else and you should read the logs to determine what is the culprit.Just note: the ERR! message for the npm publish can often be deceiving and the problem will likely mean a step was missed. Refer to the docs if you are still confused or contact the author of this confluence doc.

If pipeline succeeds, navigate to Packages & Registries to ensure your package exists

- Packages & Registries → Package Registry

Congrats! You now have a Gitlab package! The fun is just beginning.  

